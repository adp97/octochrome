class Preference{

    constructor(){
        this.fileConfiguration= this.chargeFileConfiguration();
        var puto = undefined;
        puto= localStorage.getItem("octoChrome_language");
        if(!puto){
            localStorage.setItem('octoChrome_language', this.fileConfiguration.default_language);
            puto = this.fileConfiguration.default_language;
        }
        this.fileLanguage = this.chargeFileLanguage(puto);
    }

    chargeFileConfiguration(){
        let jsons = null;
        jsons = $.ajax({
            'async': false,
            'global': false,
            'url': "./../configuration.json",
            'dataType': "json",
            'success': function (data) {
                jsons = data;


            }
        });
        return jsons.responseJSON;
    }

    chargeFileLanguage(languageTarget={type: String}){
        
        let json = null;
        let languageS = null;
        languageS = this.fileConfiguration.languages;
        let languageN = null;

        for(let i = 0; i < languageS.length; i++){
            if (languageS[i].name === languageTarget){
                languageN = languageS[i].json;
            }
        }

        if(languageN === null) return null;

        json = $.ajax({
            'async': false,
            'global': false,
            'url': "./../language/" + languageN,
            'dataType': "json",
            'success': function (data) {
                json = data;
            },
            error:function(){
                json = undefined;
            }
        });

        return json.responseJSON;
    }

    getFileLanguage(){
        return this.fileLanguage;
    }

    setLanguage(languageTarget={type: String}){
        let language = this.chargeFileLanguage(languageTarget);
        if(language === null || language === undefined) return;
        this.fileLanguage = language;
    }
}

class Printed{
    constructor(url = {type: String}, user = {type: String}, password = {type: String}, apiKey = {type: String}){
        

        this.url = url;
        this.user = user;
        this.password = password;
        this.apiKey = apiKey;
        this.conexionStatus= false;
        this.status = null;
        this.intervalUpdate= null;
        
        this.PSU = null;
        this.connection = null;
        
    }

    connect(success, error){
        let printed = this;
        $.ajax({
            url: this.url + "/api/login",
            beforeSend: function(xsh){
                xsh.setRequestHeader('X-Api-Key', this.apiKey);
                $('section#pluginOctoChrome').empty();
                $('section#pluginOctoChrome').load("./charge.html");
            },
            type: "POST",
            dataType: "json",
            data: JSON.stringify({
                passive:false,
                user: this.user,
                pass: this.password,
                remember: false
            }),
            contentType: "application/json; charset=UTF-8",
            success: success,
            error: error
        }).then(()=>{
            this.conexionStatus= true;
        }).catch(()=>{
            this.conexionStatus= false;
        });
    }

    update(){
        $.ajax({
            url: this.url +"/api/plugin/psucontrol",
            type: "GET",
            beforeSend: function(cabecera){cabecera.setRequestHeader('X-Api-Key', this.apiKey);},
            success: function(body){
                return body;
            }
        }).then((body)=>{
            this.PSU = body.isPSUOn;
        }).catch(()=>{
            this.PSU = null;
        });
        
        $.ajax({
            url: localStorage.getItem("octoApi-url")+"/api/connection",
            type: "GET",
            beforeSend: function(cabecera){cabecera.setRequestHeader('X-Api-Key', localStorage.getItem("octoApi-key"));},
            success: function(body){
                return body; 
            }
        }).then((body)=>{
            this.connection = body;
            console.log(this.connection)
        }).catch(()=>{
            
        });


        if(this.connection != null && this.connection.current.state === "Operational"){
            $.ajax({
                url: this.url +"/api/printer",
                type: "GET",
                beforeSend: function(cabecera){cabecera.setRequestHeader('X-Api-Key', this.apiKey);},
                success: function(body){
                    return body;
        
                }
        
            }).then((body)=>{
                this.status = body;
            }).catch(()=>{
                
            });
        }

    }

    getConnection(){
        return this.connection;
    }
    getStatus(){
        return this.status;
    }
    getPsuStatus(){
        return this.PSU;
    }

    psuToogle(){
        if(this.PSU){
            this.psuOff();
        }else{
            this.psuOn();
        }
    }

    psuOff(){
        let opcion = true;
        /*if(printed.connection != null && printed.connection.current.state === "Operational"&&(printed.status.state.flags.printing)){
            opcion = confirm("La impresora esta en medio de una impresion. ¿Estas seguro de que quieres apagarla?");
        }*/
    
        if(opcion){
            $.ajax({
                url: this.url + "/api/plugin/psucontrol",
                beforeSend: function(cabecera){ cabecera.setRequestHeader('X-Api-Key', this.apiKey);},
                type: "POST",
                dataType: "json",
                data: JSON.stringify({
                    command: "turnPSUOff"
                }),
                contentType: "application/json; charset=UTF-8",
                
            })
        }
    }

    psuOn(){
        $.ajax({
            url: this.url + "/api/plugin/psucontrol",
            beforeSend: function(cabecera){ cabecera.setRequestHeader('X-Api-Key', this.apiKey);},
            type: "POST",
            dataType: "json",
            data: JSON.stringify({
                command: "turnPSUOn"
            }),
            contentType: "application/json; charset=UTF-8",
        });
        
    }

}