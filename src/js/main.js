var printed;
var interval
var lang;
var interval;
function writeRegister(){
    
    $('section#pluginOctoChrome').empty();
    $('section#pluginOctoChrome').load('./registerPage.html',()=>{

        
        $('#url').attr('placeholder', lang.login_url);
        $('#user').attr('placeholder', lang.login_user);
        $('#password').attr('placeholder', lang.login_password);
        $('#apiKey').attr('placeholder', lang.login_apikey);
        $('#btnRegister').empty();
        $('#btnRegister').append(lang.login_button_login)
        

        $('#register').on("submit", (e)=>{
            e.preventDefault();
            localStorage.setItem("octoChrome-url", $('#url').val());
            localStorage.setItem("octoChrome-user", $('#user').val());
            localStorage.setItem("octoChrome-password", $('#password').val());
            localStorage.setItem("octoChrome-apikey", $('#apiKey').val());
            iniciarConexion();
        });
    });
}

function iniciarConexion(){
    printed = new Printed(
                localStorage.getItem("octoChrome-url"),
                localStorage.getItem("octoChrome-user"),
                localStorage.getItem("octoChrome-password"), 
                localStorage.getItem("octoChrome-apikey"));
    console.log(printed)

    printed.connect((body)=>{
        $('section#pluginOctoChrome').empty();
        $('section#pluginOctoChrome').load("./informationPage.html", onChargeInformationPage);
    },
    (error)=>{
        alert(lang.err_login);
        writeRegister();
    });
}




function onChargeInformationPage(){
    interval = setInterval(updatePrinted, 1000);

    //insertamos todos los textos
    $('#octoChrome-hoted').children('.title').empty();
    $('#octoChrome-hoted').children('.title').append(lang.information_tool0+" ");
    $('#octoChrome-bed').children('.title').empty();
    $('#octoChrome-bed').children('.title').append(lang.information_bed+" ");

    $('#octoChrome-psu').empty();
    $('#octoChrome-psu').append(lang.information_button_on);
    $('#octoChrome-logOut').empty();
    $('#octoChrome-logOut').append(lang.information_button_logOut);


    //Creamos todos los eventos
    $('#octoChrome-psu').on('click',()=>{
        printed.psuToogle();
        
    });
    $('#octoChrome-logOut').on('click',()=>{
        clearInterval(interval);
        writeRegister();
    });

}

function updatePrinted(){
    printed.update();
    
    if(printed.getPsuStatus() === true){
        $('#octoChrome-psu').attr("disabled", false);
        $('#octoChrome-psu').empty();
        $('#octoChrome-psu').append(lang.information_button_off);
        $("#octoChrome-psu").removeClass("btn-success");
        $("#octoChrome-psu").addClass("btn-danger");


    }else if(printed.getPsuStatus() === false){
        $('#octoChrome-psu').attr("disabled", false);
        $('#octoChrome-psu').empty();
        $('#octoChrome-psu').append(lang.information_button_on);
        $("#octoChrome-psu").removeClass("btn-danger");
        $("#octoChrome-psu").addClass("btn-success");
    }else if(printed.getPsuStatus() === null){
        $('#octoChrome-psu').attr("disabled", true);
        
    }

    let status = printed.getStatus();
    if(status!= null){
        $('#octoChrome-hoted').children('.temp').empty();
        $('#octoChrome-hoted').children('.temp').append(status.temperature.tool0.actual+"/"+status.temperature.tool0.target);
        $('#octoChrome-bed').children('.temp').empty();
        $('#octoChrome-bed').children('.temp').append(status.temperature.bed.actual+"/"+status.temperature.bed.target);
    }else{
        $('#octoChrome-hoted').children('.temp').empty();
        $('#octoChrome-hoted').children('.temp').append("Null");
        $('#octoChrome-bed').children('.temp').empty();
        $('#octoChrome-bed').children('.temp').append("Null");
    }
}

$(document).ready(function () {
    var preferencias = new Preference();
    lang = preferencias.getFileLanguage();

    if(!localStorage.getItem("octoChrome-key") && !localStorage.getItem("octoChrome-url") && !localStorage.getItem("octoChrome-user") && !localStorage.getItem("octoChrome-password")){
        writeRegister();
    }else{
        iniciarConexion();
    }
});




/*var printed = {connection: null, psuOn: null, status: null};
var interval;

const conectar = ()=>{
    console.log("conectando con el servidor")
    $.ajax({
        url: localStorage.getItem("octoApi-url") + "/api/login",
        beforeSend: function(cabecera){cabecera.setRequestHeader('X-Api-Key', localStorage.getItem("octoApi-key"));},
        type: "POST",
        dataType: "json",
        data: JSON.stringify({
            passive:false,
            user: localStorage.getItem("octoApi-user"),
            pass: localStorage.getItem("octoApi-password"),
            remember: false
        }),
        contentType: "application/json; charset=UTF-8",

        success: function(body){
            writeInformation();
        },
        error: function() {
            alert("No se ha podido acceder al servidor");
            writeRegister();
        }
    });

}




function writeRegister(){
    $('section#pluginOctoChrome').empty();
    $('section#pluginOctoChrome').load('./registerPage.html',()=>{
        $('#register').on("submit", (e)=>{
            e.preventDefault();
            localStorage.setItem("octoApi-key", $('#apiKey').val());
            localStorage.setItem("octoApi-url", $('#url').val());
            localStorage.setItem("octoApi-user", $('#user').val());
            localStorage.setItem("octoApi-password", $('#password').val());
            conectar();
        });
    });
}

$(document).ready(function () {
    if(!localStorage.getItem("octoApi-key") && !localStorage.getItem("octoApi-url") && !localStorage.getItem("octoApi-user") && !localStorage.getItem("octoApi-password")){
        writeRegister()
    }else{
        conectar();
    }
});

function writeInformation(){
    $('section').empty();
    $('section').load('./informationPage.html',()=>{
        if(interval) clearInterval(interval);
        interval = setInterval(cargarInformacion, 1000);

        $('#apagar').on("click", (e)=>{
            e.preventDefault();
            powerToogle();
        });
    });
}

const powerToogle= ()=>{
    if(printed.psuOn){
        powerOff();
    }else{
        powerOn();
    }
}
const powerOff = ()=>{
    let opcion = true;
    if(printed.connection != null && printed.connection.current.state === "Operational"&&(printed.status.state.flags.printing)){
        opcion = confirm("La impresora esta en medio de una impresion. ¿Estas seguro de que quieres apagarla?");
    }

    if(opcion){
        $.ajax({
            url: localStorage.getItem("octoApi-url") + "/api/plugin/psucontrol",
            beforeSend: function(cabecera){ cabecera.setRequestHeader('X-Api-Key', localStorage.getItem("octoApi-key"));},
            type: "POST",
            dataType: "json",
            data: JSON.stringify({
                command: "turnPSUOff"
            }),
            contentType: "application/json; charset=UTF-8",
            
        })
    }
}

const powerOn = ()=>{
    $.ajax({
        url: localStorage.getItem("octoApi-url") + "/api/plugin/psucontrol",
        beforeSend: function(cabecera){ cabecera.setRequestHeader('X-Api-Key', localStorage.getItem("octoApi-key"));},
        type: "POST",
        dataType: "json",
        data: JSON.stringify({
            command: "turnPSUOn"
        }),
        contentType: "application/json; charset=UTF-8",
    });
    
}


var cargarInformacion = ()=>{
    //compruebo si la impresora esta conectada
    $.ajax({
        url: localStorage.getItem("octoApi-url")+"/api/connection",
        type: "GET",
        beforeSend: function(cabecera){cabecera.setRequestHeader('X-Api-Key', localStorage.getItem("octoApi-key"));},
        success: function(body){
            printed.connection = body; 
        },
        error:function(){
            writeRegister();
            if(interval) clearInterval(interval);
        }
    });

    //compruebo si la impresora esta encendida
    $.ajax({
        url: localStorage.getItem("octoApi-url")+"/api/plugin/psucontrol",
        type: "GET",
        beforeSend: function(cabecera){cabecera.setRequestHeader('X-Api-Key', localStorage.getItem("octoApi-key"));},
        success: function(body){
            printed.psuOn = body.isPSUOn;
            
        }
    });

    //extraigo toda la informacion
    if(printed.connection != null && printed.connection.current.state === "Operational"){
        $.ajax({
            url: localStorage.getItem("octoApi-url")+"/api/printer",
            type: "GET",
            beforeSend: function(cabecera){cabecera.setRequestHeader('X-Api-Key', localStorage.getItem("octoApi-key"));},
            success: function(body){
                printed.status = body;
                let informacion = $('#informacion');
                informacion.empty();
                informacion.append(
                    `Emperatura hotend: ${body.temperature.tool0.actual} / ${body.temperature.tool0.target}`
                );
    
            }
    
        });
    }
    console.log(printed)
}*/
